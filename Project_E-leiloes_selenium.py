from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support import expected_conditions as EC
import time
import sys
import csv
import pandas as pd
import math



#****************************JSON**************************

array_json=[]
import os.path #Biblioteca que vai ser usada na função controlo_ficheiro_json() que verifica se o documento está criado ou não
import json
import os
diretoria_json='D:\\leiloes.json'

#*************************************
# Instalar no terminal :
# pandas: pip install pandas
# selenium: pip install selenium
#*************************************

url = 'https://www.e-leiloes.pt/pesquisaavancada.aspx'
#url = 'https://www.e-leiloes.pt/'
chrome_driver_path = '/Users/User/Desktop/selenium_project/chromedriver'
chrome_options = Options()
chrome_options.add_argument('--headless')

webdriver = webdriver.Chrome(
  executable_path=chrome_driver_path, options=chrome_options
)

#default search query
search_city = "Faro"
search_tipo ="Apartamento"
  
if (len(sys.argv) >= 2):
  search_city = sys.argv[1]
  #print(search_city)

if (len(sys.argv) >= 2):
  search_tipo = sys.argv[1]
  #print(search_tipo)

with webdriver as driver:
    # Set timeout time 
    wait = WebDriverWait(driver,5)

    # retrive url in headless browser
    driver.get(url)
      
    #print(driver.current_url)
    select= Select(driver.find_element_by_id("CPH_Body_P_Subtipo"))
    opcoes_tipo=[]
    for opt in select.options:
        opcoes_tipo.append(opt.text)
    del opcoes_tipo[0]
    #print(opcoes_tipo)
    conta_imovel=0
    for conta_opcoes_tip in range(len(opcoes_tipo)):
          
          #print(opcoes_tipo[conta_opcoes_tip])         
          driver.find_element_by_id("CPH_Body_P_Subtipo").send_keys(opcoes_tipo[conta_opcoes_tip])
          driver.find_element_by_id("CPH_Body_P_Distrito").send_keys(search_city)
          driver.find_element_by_id("CPH_Body_BT_Pesquisar").send_keys(Keys.RETURN)        
          wait.until(presence_of_element_located((By.ID, "CPH_Body_Panel_Conteudo")))
          time.sleep(3)
          bens_encontrados = driver.find_element_by_id('CorpoConteudoPagina')
          valor_encontados=bens_encontrados.text.split()
          valor_total_dos_bens=int(valor_encontados[1])
          #print(valor_total_dos_bens)
          valor_total_dos_bens_arredondado=((valor_total_dos_bens-4)/4)

          maximo_de_datamore=math.ceil(valor_total_dos_bens_arredondado)
          #print(maximo_de_datamore)

          results_retorna=0
          valor_retorna=0
          data_retorna=0
          results_init = driver.find_elements_by_class_name('PaginacaoBemTextos2Titulo')
          #print('numero de imoveis :', valor_total_dos_bens)
          #print('maximo de carregamentos',maximo_de_datamore)
          
          if maximo_de_datamore>0 and len(results_init)>0:
            x=0
            for x in range(maximo_de_datamore):
              #print('Carregamento numero',x) 
              driver.find_element_by_id('DataMore').find_elements_by_tag_name('span')[0].click()
              time.sleep(5)
              results = driver.find_elements_by_class_name('PaginacaoBemTextos2Titulo')
              valor = driver.find_elements_by_class_name('PaginacaoBemLanceAtual')
              data = driver.find_elements_by_class_name('PaginacaoBemCabecalho02')
              results_retorna=results
              valor_retorna=valor
              data_retorna=data
            
            #print('resultados total de imovel', len(results_retorna))
            for contador in range(0,len(results_retorna)): 
              conta_imovel+=1
              print(conta_imovel)
              var_results = results_retorna[contador].text.strip('\n')
              print(var_results)
              var_valor = valor_retorna[contador].text.strip('\n')
              print(var_valor)
              var_data = data_retorna[contador].text.strip('\n')
              print(var_data)
              print(opcoes_tipo[conta_opcoes_tip]) 
              print('\n')

            #***************************JSON*******************
              cli={'Tipo':opcoes_tipo[conta_opcoes_tip],'Texto':var_results,'valor':var_valor,'data':var_data}
              array_json.append(cli)
            #**************************************************


          elif maximo_de_datamore<1 and len(results_init)>0:
                valor = driver.find_elements_by_class_name('PaginacaoBemLanceAtual')
                data = driver.find_elements_by_class_name('PaginacaoBemCabecalho02')
                valor_retorna=valor
                data_retorna=data
                #contador=0
                #print('resultados', len(results_init))
                for contador in range(0,len(results_init)):   
                  conta_imovel+=1             
                  print(conta_imovel)
                  var_results = results_init[contador].text.strip('\n')
                  print(var_results)
                  var_valor = valor_retorna[contador].text.strip('\n')
                  print(var_valor)
                  var_data = data_retorna[contador].text.strip('\n')
                  print(var_data)
                  print(opcoes_tipo[conta_opcoes_tip]) 
                  print('\n')

                #***************************JSON*******************
                  cli={'Tipo':opcoes_tipo[conta_opcoes_tip],'Texto':var_results,'valor':var_valor,'data':var_data}
                  array_json.append(cli)
                #**************************************************


                  
          elif  maximo_de_datamore==0 and len(results_init)==0:
            print('nao tem dados para amostrar')
          driver.back()
          driver.refresh()

        #*************************Grava um doc json****************************

          data = {}
          data['cliente'] = array_json # Cria a key cliente para cada array e guarda no dictionary data
          with open(diretoria_json, 'w') as outfile:
            json.dump(data, outfile)

        #******************************************************************

    driver.close()       
      
    

